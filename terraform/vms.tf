variable "testvm_password" {
    type        = string
    sensitive   = true
    default     = ""
}

variable "testvm_user" {
    type        = string
    sensitive   = true
    default     = ""
}

resource "proxmox_vm_qemu" "testvm" {
    vmid        = "501"
    name        = "testvm"
    target_node = "pve"
    desc        = "gterraform test deployment"
    os_type     = "cloud-init"
    ciuser      = var.testvm_user
    cipassword  = var.testvm_password
    clone       = "rhel-8-tmpl"
    onboot      = false
    agent       = 1
    cores       = 2
    sockets     = 1
    cpu         = "host"
    memory      = 2048
    balloon      = 1024
    network {
        bridge = "vmbr0"
        model = "virtio"
    }
    ipconfig0   = "ip=dhcp"
}