# Infrastructure as code for the homelab

## Contents

- **packer** - Contains template configurations and KS files for hashicorp packer
    - **centos-stream-9**
    - **rhel8** Deploys a minimal RHEL 8.8 Template to proxmox
- **terraform** - Contains tf files to deploy VMs on proxmox

