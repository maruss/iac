# RHEL 8
# ---
# Packer template to create a RHEL 8 server on Proxmox

# Variable definitions
variable "proxmox_api_url" {
    type = string
}

variable "proxmox_username" {
    type = string
}

variable "proxmox_password" {
    type = string
    sensitive = true
}

variable "vmid" {
    type = string
}

variable "ksserver" {
    type = string
}

# Resource definition for the VM template
source "proxmox-iso" "rhel-8"{

    # Proxmox connection
    proxmox_url = "${var.proxmox_api_url}"
    username = "${var.proxmox_username}"
    token = "${var.proxmox_password}"
    #insecure_skipr_tls_verify = true

    # General VM settings
    node = "pve"
    vm_id = "${var.vmid}"
    vm_name = "rhel-8-tmpl"
    template_description = "Basic RHEL 8 Server"

    # VM OS settings
    iso_file = "isostore:iso/rhel-8.8-x86_64-dvd.iso"
    #iso_url = "https://mirror.stream.centos.org/9-stream/BaseOS/x86_64/iso/CentOS-Stream-9-latest-x86_64-boot.iso"
    iso_storage_pool = "isostore"
    unmount_iso = true

    # VM settings
    qemu_agent = true

    # VM HDD settings
    scsi_controller = "virtio-scsi-pci"

    disks {
        disk_size = "20G"
        format = "raw"
        storage_pool = "tank"
        storage_pool_type = "zfs"
        type = "sata"
    }

    # VM CPU settings
    cpu_type = "host"
    cores = "2"

    # VM memory settings
    memory = "2048"

    # VM network settings
    network_adapters {
        model = "virtio"
        bridge = "vmbr0"
        firewall = "false"
    }

    # Cloud-init settings
    cloud_init = true
    cloud_init_storage_pool = "tank"

    # PACKER boot commands
    boot_command = [
        "<wait2><up>",
        "<esc><wait2>",
        "",
        "linux inst.repo=cdrom inst.ks=${var.ksserver}/rhel8.ks <enter>",
    ]

    boot = "c"

    ssh_username = "packer"
    ssh_password = "packertemplate"
    ssh_timeout = "30m"
}

# Build definition to create the VM template
build {
    name = "rhel-8"
    sources = ["source.proxmox-iso.rhel-8"]

    provisioner "shell" {
        inline = [
            "sudo rm /etc/ssh/ssh_host_*",
            "sudo truncate -s 0 /etc/machine-id",
            "sudo useradd cloud-init",
            "sudo cloud-init clean",
            "sudo sync"
            ]
    }

    # Provisioning the VM template for cloud-init integration
    provisioner "file" {
        source = "files/99-pve.cfg"
        destination = "/tmp/99-pve.cfg"
    }

    # todo: try userdel instead of mod
    provisioner "shell" {
        inline = [ "sudo cp /tmp/99-pve.cfg /etc/cloud/cloud.cfg.d/99-pve.cfg",
                   "sudo localectl set-keymap de-latin1-nodeadkeys",
                   "sudo usermod -s /usr/sbin/nologin packer" ]
    }
}