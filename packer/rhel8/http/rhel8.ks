lang de_DE
keyboard no
timezone Europe/Berlin --isUtc
rootpw CHANGEME --iscrypted
#platform x86, AMD64, or Intel EM64T
reboot
text
cdrom
bootloader --location=mbr --append="rhgb quiet crashkernel=auto"
zerombr
clearpart --all --initlabel
autopart
auth --passalgo=sha512 --useshadow
user --name=packer --password=packertemplate
#sshkey --username=username "ssh-rsa KEY###"
selinux --disabled
firewall --enabled
skipx
firstboot --disable
%post
#usermod -aG wheel packer
echo "packer ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/packer
sed 's/^PasswordAuthentication no/PasswordAuthentication yes/g' -i /etc/ssh/sshd_config
sed 's/^PermitRootLogin yes/PermitRootLogin no/g' -i /etc/ssh/sshd_config
dnf update -y 
%end
%packages
#@core
@^minimal-environment
qemu-guest-agent
openssh-server
cloud-init
#@^server-product-environment
#@development
%end
