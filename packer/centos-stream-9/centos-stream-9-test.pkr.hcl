# CentOS Stream 9
# ---
# Packer template to create a CentOS 9 server on Proxmox

# Variable definitions
variable "proxmox_api_url" {
    type = string
}

variable "proxmox_api_token_id" {
    type = string
}

variable "proxmox_api_token_secret" {
    type = string
    sensitive = true
}

# Resource definition for the VM template
source "proxmox" "centos-stream-9"{

    # Proxmox connection
    proxmox_url = "${var.proxmox_api_url}"
    username = "${var.proxmox_api_token_id}"
    token = "${var.proxmox_api_token_secret}"
    #insecure_skipr_tls_verify = true

    # General VM settings
    node = "pve"
    vm_id = "901"
    vm_name = "centos-stream-9-tmpl"
    template_description = "Basic CentOS Stream 9 Server"

    # VM OS settings
    iso_file = "local:iso/CentOS-Stream-9-latest-x86_64-dvd1.iso"
    #iso_url = "https://mirror.stream.centos.org/9-stream/BaseOS/x86_64/iso/CentOS-Stream-9-latest-x86_64-boot.iso"
    iso_storage_pool = "local"
    unmount_iso = true

    # VM settings
    qemu_agent = true

    # VM HDD settings
    scsi_controller = "virtio-scsi-pci"

    disks {
        disk_size = "20G"
        format = "raw"
        storage_pool = "tank"
        storage_pool_type = "zfs"
        type = "sata"
    }

    # VM CPU settings
    cpu_type = "host"
    cores = "2"

    # VM memory settings
    memory = "2048"

    # VM network settings
    network_adapters {
        model = "virtio"
        bridge = "vmbr1"
        firewall = "false"
    }

    # Cloud-init settings
    cloud_init = true
    cloud_init_storage_pool = "tank"

    # PACKER boot commands
    boot_command = [
        "<wait5><up>",
        "<esc><wait5>",
        "linux inst.repo=cdrom inst.ks=http://10.0.0.18:8000/centos.ks <enter>"
    ]

    boot = "c"

    ssh_username = "cloud-user"
    ssh_password = "init"
    ssh_timeout = "15m"
}

# Build definition to create the VM template
build {
    name = "centos-stream-9"
    sources = ["source.proxmox.centos-stream-9"]

    provisioner "shell" {
        inline = [
            "sudo rm /etc/ssh/ssh_host_*",
            "sudo truncate -s 0 /etc/machine-id",
            "sudo yum -y update",
            "sudo yum -y clean all",
            "sudo cloud-init clean",
            "sudo sync"
            ]
    }

    # Provisioning the VM template for cloud-init integration
    provisioner "file" {
        source = "files/99-pve.cfg"
        destination = "/tmp/99-pve.cfg"
    }

    provisioner "shell" {
        inline = [ "sudo cp /tmp/99-pve.cfg /etc/cloud/cloud.cfg.d/99-pve.cfg" ]
    }
}